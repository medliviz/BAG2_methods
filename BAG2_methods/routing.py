"""
BAG2 methods --- routing.py
======================

Helper functions for instance routing in layout generators
written with the Berkeley Analog Generator (BAG) framework.

Created on 09.03.2021 by Santeri Porrasmaa, santeri.porrasmaa@aalto.fi

"""

from bag.layout.routing.base import WireArray
from bag.layout.util import BBox 

class routing_helper():
    """
    Helper class to aggregate helper functions for routing.

    Parameters
    ----------

    grid : bag.layout.routing.RoutingGrid
        The grid used in the template
    tech_info : bag.layout.core.TechInfo
        TechInfo class used to access process specific parameters

    """
    def __init__(self, grid):
        self.grid = grid 
        self.tech_info = grid.tech_info

    def sort_wires(self, warrs):
        '''
        Sort wires of same direction in ascending order.
        '''
        warr_dirs = set([self.grid.get_direction(warr.track_id.layer_id) for warr in warrs])
        if len(warr_dirs)>1:
            raise ValueError("Sort wires works only on WireArrays of same routing direction")
        return sorted(warrs, key=lambda w: self.grid.track_to_coord(w.track_id.layer_id,
            w.track_id.base_index, unit_mode=True))

    def get_closest_available_track(self, template, lay, tid, lower, upper, width=1, unit_mode=True, half_tracks=False, max_iters=100, margin=0):
        '''
        Get closest available track with respect to tid by searcing to the positive and negative
        directions. Search scope is limited by max_iters, so that each direction of the search
        will progress max_iters / 2 tracks from the original track (hence searching max_iters tracks
        in total).

        Returns the track_id which is available if found, otherwise returns None.

        Parameters:
        -----------
        
        template: TemplateBase
            the layout template to find the track in
        lay: int
            Layer ID of track
        tid: int, float
            The original track index
        lower: int,float
            The lower coord of desired track
        upper: int,float
            The upper coord of desired track
        width: int
            width of track in number of tracks
        unit_mode: bool
            lower, upper given in resolution units?
        half_tracks: bool
            Allow iteration on half tracks?
        max_iters: int
            Search by max_iters / 2 tracks to either direction
        margin: int
            Desired number of tracks between tid and possible other wires
        '''
        margin = margin*self.grid.get_track_pitch(lay,unit_mode=unit_mode)
        count = 1
        dir=True
        step = 0.5 if half_tracks else 1
        tid_orig = tid
        while not template.is_track_available(lay, tid, lower, upper, width=width, unit_mode=unit_mode,sp=margin) and count < max_iters//2 + 1:
            if dir:
                tid=tid_orig + count*step
            else:
                tid=tid_orig - count*step
                count += 1
            dir = not dir
        # Check that we actually found the tracks or if iteration ended due to max_iters constraint.
        if template.is_track_available(lay, tid, lower, upper, width=width, unit_mode=unit_mode):
            return tid
        else:
            return None

    def get_min_via_enc(self, top_lay, wire_w, ext_dir):
        '''
        Minimize via size orthogonal to extension direction
        '''
        via_name = self.tech_info.get_via_name(top_lay-1)
        via_config = self.tech_info.config['via']['via_units'][via_name].square
        #dim = via_config['dim']
        enc = via_config['top_enc']
        dim = via_config['dim']
        sp = via_config['sp']
        for w, enct in zip(enc['w_list'], enc['enc_list']):
            if wire_w <= w:
                break
        #via_ext_dir = 'x' if dir=='x' else 'y' 
        min_idx = 0 if ext_dir == 'y' else 1

        if isinstance(sp[0], list):
            sp_min = min(sp, key=lambda t: t[min_idx])
        else:
            sp_min = sp
        if isinstance(dim[0], list):
            dim_min = min(dim, key=lambda t: t[min_idx])
        else:
            dim_min = dim
        enc_min = min(enct, key=lambda t: t[min_idx])
        return dim_min, sp_min, enc_min


    def connect_helper(self, template, warr, track_id, num_via, ext_mode=0):
        '''
        Helper function to make multicut via connection between warr and track_id.

        Parameters
        ----------
        warr: WireArray
            WireArray to connect to given track_id
        track_id: TrackID
            TrackID to connect to
        num_via: int
            Number of vias to make
        ext_mode: int
            Extension direction of via enclosure to fit num_via. By default,
            extend to both directions along track_id layer direction. If ext_mode
            -1, extend to minus direction (left or down). If ext_mode 1, extend
            to positive direction (right or up).

        '''
        lay1 = warr.track_id.layer_id
        lay2 = track_id.layer_id
        res = self.grid.resolution
        if self.grid.get_direction(lay1) == self.grid.get_direction(lay2):
            raise ValueError("Connect helper works only on orthogonal layers!")
        if abs(lay1-lay2) > 1:
            raise ValueError("Connect helper works only for adjacent layers!")
        dir_warr = self.grid.get_direction(lay1)
        dir_tid = self.grid.get_direction(lay2)
        top_lay = max(lay1, lay2)
        bot_lay = min(lay1, lay2)
        top_lay_name = self.tech_info.get_layer_name(top_lay)
        bot_lay_name = self.tech_info.get_layer_name(bot_lay)
        boxarr = warr.get_bbox_array(self.grid)
        via_boxes = []
        for box in boxarr:
            left, right = (box.left_unit, box.right_unit) if dir_warr == 'y' else track_id.get_bounds(self.grid,True)
            bot, top = track_id.get_bounds(self.grid, True) if dir_warr=='y' else box.bottom_unit, box.top_unit
            via_ext_dir = dir_warr 
            wire_w = box.width_unit if dir_warr == 'y' else box.height_unit
            dim,sp,enc=self.get_min_via_enc(top_lay, wire_w, via_ext_dir)
            if via_ext_dir == 'x':
                via_box_w = num_via*(dim[0]+sp[0]) - sp[0] + 2*enc[0]
                ext = via_box_w - wire_w
                left -= ext // 2
                right += ext // 2
                top += enc[1]
                bot -= enc[1]
            else:
                via_box_h = num_via*(dim[1]+sp[1]) - sp[1] + 2*enc[1]
                ext = via_box_h - wire_w
                bot -= ext // 2
                top += ext // 2
                left -= enc[0]
                right += enc[0]
            via_boxes.append(BBox(left, bot, right, top, res, True))
            template.add_via(via_boxes[-1], bot_lay_name, top_lay_name, bot_dir=dir_warr, top_dir=dir_warr, extend=False)
            template.mark_bbox_used(top_lay, via_boxes[-1])
            template.mark_bbox_used(bot_lay, via_boxes[-1])
        if dir_tid =='y':
            lower = min(via_boxes, key=lambda box: box.bottom_unit).bottom_unit
            upper = max(via_boxes, key=lambda box: box.top_unit).top_unit
        else:
            lower = min(via_boxes, key=lambda box: box.left_unit).left_unit
            upper = max(via_boxes, key=lambda box: box.right_unit).right_unit
        return template.add_wires(track_id.layer_id, track_id.base_index, lower, upper,
                track_id.width, track_id.num, track_id.pitch, unit_mode=True)

    def get_parallel_run_length(self, wire_lower, wire_upper, adj_wire, unit_mode=False):
        """ 
        Helper function to find the parallel run length between a wire to be
        drawn and a wire adjacent to that wire. Assumes that wires are routed
        in the same direction.
        
        Parameters
        ----------
        wire_lower: Union[int, float]
            Lower coordinate of wire to be drawn
        wire_upper : Union[int, float]
            Upper coordinate of wire to be drawn
        adj_wire : WireArray
            WireArray adjacent to the one being drawn
        unit_mode : boolean
            If true, upper and lower coordinates were given in resolution
            units
        
        Returns
        -------
        par_len : Union[int, float]
            Parallel run length of the two wires
        """
        grid = self._grid
        res = grid.resolution
        if not isinstance(adj_wire, WireArray):
            raise Exception('Invalid type %s for argument adj_wires!' % type(adj_wires))
        if not unit_mode:
            wire_upper = int(wire_upper/res)
            wire_lower = int(wire_lower/res)
        if wire_upper < wire_lower:
            raise Exception('Non-physical wire end points given (e.g. upper < lower)!')
        par_len = min(wire_upper, adj_wire.upper_unit) - max(wire_lower, adj_wire.lower_unit) 
        if par_len < 0: # If the run length is negative, the y-projections do not overlap
            par_len = 0
        return par_len if unit_mode else par_len * res

    def get_parallel_run_length_primitive(self, wire_lower, wire_upper, adj_box, direction, unit_mode=False):
        """
        Helper function to find the parallel run length between a wire to be
        drawn and a primitive wire adjacent to that wire.

        Parameters
        ----------
        wire_lower: Union[int, float]
            Lower coordinate of wire to be drawn
        wire_upper : Union[int, float]
            Upper coordinate of wire to be drawn
        adj_box : BBox 
            Bounding box of the primitive wire 
        direction : str
            Routing direction, either 'x' or 'y'.
        unit_mode : boolean
            If true, upper and lower coordinates were given in resolution
            units
        Returns:
        --------
        par_len : Union[int, float]
            Parallel run length of the two wires
        """
        grid = self._grid
        res = grid.resolution
        valid_dirs = set(['x', 'y'])
        if not isinstance(adj_box, BBox):
            raise Exception('Invalid type %s for argument adj_box!' % type(adj_box))
        if direction not in valid_dirs:
            raise Exception('Direction must be either x or y!')
        if wire_upper < wire_lower:
            raise Exception('Non-physical wire end points given (e.g. upper < lower)!')
        if not unit_mode:
            wire_upper = int(wire_upper/res)
            wire_lower = int(wire_lower/res)
        if direction == 'x':
            par_len = min(wire_upper, adj_box.right_unit) - max(wire_lower, adj_box.left_unit) 
        else:
            par_len = min(wire_upper, adj_box.top_unit) - max(wire_lower, adj_box.bottom_unit) 
        if par_len < 0: # If the run length is negative, the y-projections do not overlap
            par_len = 0
        return par_len if unit_mode else par_len * res
         
    def get_min_space_parallel(self, layer_id, width, length, same_color=False, unit_mode=False):
        """
        Helper function to find minimum spacing between two parallel wires

        Parameters
        ----------
        layer_id : int
            routing grid layer id
        width : Union[int, float]
            maximum width of two parallel wires 
        length : Union[int, float]
            parallel run length of two parallel wires 
        same_color : boolean 
            True to use same-color spacing 
        unit_mode : boolean
            If true, width and length were given in resolution
            units

        Returns:
        --------
        sp : Union[int, float]
            minimum required spacing between two parallel wires 
        """

        tech_info = self._tech_info
        parfunc = getattr(tech_info, 'get_parallel_space', None)
        if not callable(parfunc):
            raise Exception('Cannot find parallel spacing! Function get_parallel_space not in tech.def!')
        lay_name = tech_info.get_layer_name(layer_id)
        if isinstance(lay_name, tuple):
            lay_name = lay_name[0]
        lay_type = tech_info.get_layer_type(lay_name)
        return tech_info.get_parallel_space(lay_type, width, length, same_color, unit_mode)
