#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import *
from operator import attrgetter
import unittest

from sal.row import *

#-------------------------------------- Enums -------------------------------------

class TransistorAlignment(Enum):
    '''
    How to align the transistor within the column/stack.
    '''
    CENTER = 0
    RIGHT = 1
    LEFT = -1

class EffectiveSource(Enum):
    S = 's'   # even diffusion region
    D = 'd'   # odd diffusion region

class TransistorDirection(Enum):
    DOWN = 0
    UP = 2
    MIDDLE = 1

#-------------------------------------- Classes -------------------------------------

class Transistor:
    '''
    Initialize the transistor data structure.
    s_net and d_net are the source and drain nets of the transistor in the schematic. i.e. it is the effective
    source and drain connections, regardless of whether the source is drawn on the even or odd diffusion regions

    Parameters
    ----------
    name : str
        name of the transistor.
    row : Row
        the row this transistor will be placed on
    fg : int
        the (integer) number of fingers in this transistor
    seff_net : Optional[str]
        the name of the effective source net for dummy calculation.
    deff_net : Optional[str]
        the name of the effective drain net for dummy calculation.
    '''
    def __init__(self,
                 name: str,
                 row: Row,
                 fg: int,
                 seff_net: Optional[str] = None,
                 deff_net: Optional[str] = None,
                 ):
        self.name = name
        self.row = row
        self.fg = fg
        self.seff_net = '' if seff_net is None else seff_net
        self.deff_net = '' if deff_net is None else deff_net

    def __align_in_column(self,
                          fg_col: int,
                          align: TransistorAlignment,
                          ) -> int:
        """
        Returns the column index offset to justify a transistor within a column/stack

        Parameters
        ----------
        fg_col : int
            width (in fingers) of the column
        fg_tx : int
            width (in fingers) of the transistor
        align : TransistorAlignment
            how to align the transistor in the column

        Returns
        -------
        col : int
            the column index of the start of the transistor
        """
        if align is TransistorAlignment.CENTER:
            col = (fg_col - self.fg) // 2
        elif align is TransistorAlignment.LEFT:
            col = 0
        elif align is TransistorAlignment.RIGHT:
            col = fg_col - self.fg
        return col

    def assign_column(self,
                      offset: int,
                      fg_col: Optional[int] = None,
                      align: TransistorAlignment = TransistorAlignment.CENTER):
        """
        Calculates and assigns the transistor's column index (the position of the leftmost finger of the transistor).
        If fg_col is passed, the transistor is assumed to be in some stack of transistors that should all be
        horizontally aligned within a given "column/stack". The offset in this case refers to the start of the column of
        transistors rather than the offset of the passed transistor

        Parameters
        ----------
        offset : int
            the offset (in column index) of the transistor, or the column (stack) the transistor is in if fg_col is
            not None
        fg_col : int
            the width in fingers of the column/stack in which the passed transistor should be aligned
        align : TransistorAlignment
            How to align the transistor within the column/stack. 0 to center, 1 to right justify, -1 to left justify.

        Returns
        -------

        """
        if fg_col is None:
            fg_col = self.fg
        self.col = offset + self.__align_in_column(fg_col, align)

    def set_directions(self,
                       seff: EffectiveSource,
                       seff_dir: TransistorDirection,
                       deff_dir: Optional[TransistorDirection] = None
                       ):
        """
        Sets the source/drain direction of the transistor. Sets the effective source/drain location
        BAG defaults source to be the left most diffusion, and then alternates source-drain-source-...
        seff specifies whether the transistors effective source should be in the 's' or 'd' position.
        seff_dir specifies whether the effective source's vertical connections will go up or down

        If not specified, drain location and direction are assigned automatically to be opposite to the source

        Note: For mirrored rows, source and drain direction are flipped by BAG

        Parameters
        ----------
        seff : EffectiveSource
            's' or 'd' to indicate whether the transistor's effective source is the odd or even diffusion regions
        seff_dir : TransistorDirection
            0 to indicate connection goes down. 2 to indicate connection goes up. 1 to indicate middle connection
        deff_dir : Optional[TransistorDirection]
            0 to indicate connection goes down. 2 to indicate connection goes up. 1 to indicate middle connection

        Returns
        -------

        """
        self.seff = seff
        # NOTE: self.deff is a dynamic @property

        self.seff_dir = seff_dir

        if deff_dir is None:
            switcher = {
                # maps       seff_dir      ->     deff_dir
                TransistorDirection.DOWN:   TransistorDirection.UP,
                TransistorDirection.UP:     TransistorDirection.DOWN,
                TransistorDirection.MIDDLE: TransistorDirection.MIDDLE,
            }
            self.deff_dir = switcher[seff_dir]
        else:
            self.deff_dir = deff_dir
    
    def set_matched_direction(self,
                              reference: Transistor,
                              seff_dir: TransistorDirection,
                              aligned: bool,
                              deff_dir: Optional[TransistorDirection] = None):
        """
        Align the source/drain position (is source or drain leftmost diffusion) to a reference transistor
        Also assign the source and drain connection directions (up/down)

        Parameters
        ----------
        reference : Transistor
            the reference transistor whose source/drain alignment should be matched
        seff_dir : TransistorDirection
            the connection direction of the effective source for the target transistor (0 for down, 2 for up,
            1 for middle)
        aligned : bool
            True to have the target transistor's s/d connection be aligned with the source transistor.
            False to have s_target align with d_source and d_target align with s_source
        deff_dir : Optional[TransistorDirection]
            if specified, sets the connection direction of the effective drain for the target transistor.
            If not specified, drain direction is assumed to be opposite to the source direction

        Returns
        -------

        """
        if (self.fg - reference.fg) % 4 == 0 and aligned is True:
            self.set_directions(seff=reference.seff, seff_dir=seff_dir, deff_dir=deff_dir)
        else:
            self.set_directions(seff=reference.deff, seff_dir=seff_dir, deff_dir=deff_dir)


    @property
    def deff(self):
        switcher = {
            EffectiveSource.S: EffectiveSource.D,
            EffectiveSource.D: EffectiveSource.S
        }
        return switcher[self.seff]

    # Based on whether the source or drain is on the even diffusion regions, assign the sdir (corresponding
    #  to the even diffusion regions) and ddir (corresponding to odd diffusion regions)

    @property
    def s_dir(self):
        switcher = {
            EffectiveSource.S: self.seff_dir,
            EffectiveSource.D: self.deff_dir
        }
        return switcher[self.seff]

    @property
    def d_dir(self):
        switcher = {
            EffectiveSource.S: self.deff_dir,
            EffectiveSource.D: self.seff_dir
        }
        return switcher[self.seff]

    # Based on whether the source or drain is on the even diffusion regions, assign the s_net name (corresponding
    #  to the even diffusion regions) and d_net name (corresponding to odd diffusion regions)

    @property
    def s_net(self):
        switcher = {
            EffectiveSource.S: self.seff_net,
            EffectiveSource.D: self.deff_net
        }
        return switcher[self.seff]

    @property
    def d_net(self):
        switcher = {
            EffectiveSource.S: self.deff_net,
            EffectiveSource.D: self.seff_net
        }
        return switcher[self.seff]

    def set_ports(self,
                  g: WireArray,
                  d: WireArray,
                  s: WireArray):
        '''
        Parameters
        ----------
        g : WireArray
            g port as WireArray
        d : WireArray
            d port as WireArray
        s : WireArray
            s port as WireArray
        '''
        self.g = g
        self.d = d
        self.s = s

#-------------------------------------- Unit Tests -------------------------------------

class TestTransistor(unittest.TestCase):
    def setUp(self):
        self.row = Row(name="n", orientation=RowOrientation.R0,
                channel_type=ChannelType.N, width=1, threshold='lvt')
        self.t1 = Transistor(name="n", row=self.row,
                fg=1, seff_net='I', deff_net='O')

    # def test_n_rows(self):
    #     rows = RowList([self.n1, self.n2, self.p1])
    #     self.assertEqual(len(rows.n_rows), 2)

if __name__ == '__main__':
    unittest.main()
