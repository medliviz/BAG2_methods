import abc
import os
import re
import shutil
import sys

import bag.core

from sal.env import Env
from sal.log import warn

class TestbenchBase(metaclass=abc.ABCMeta):
    def __init__(self, *arg):
        """ Overload if you need to refine this
        """
        pass

    @property
    def _classfile(self):
        """
        Find the path of the subclass python file, add the subclass name to the containing directory
        f.ex.
            subclass script: <path>/virtuoso_template/inverter2_gen/inverter2_gen/inverter2_gen.py
            _classfile returns: <path>/virtuoso_template/inverter2_gen/inverter2_gen/inverter2_gen
        """
        path_of_subclass = os.path.dirname(os.path.abspath(sys.modules[self.__class__.__module__].__file__))
        subclass_name = self.__class__.__name__
        return os.path.join(path_of_subclass, subclass_name)

    def print_log(self, **kwargs):
        # TODO: extract log from DesignBase to be more reusable
        type = kwargs.get('type', 'I')
        msg = kwargs.get('msg', 'Print this to the log')
        print(msg)

    def import_testbench_template(self,
                                  bag_project: bag.core.BagProject,
                                  template_library: str,
                                  cell: str):
        """
        Method to import Virtuoso templates to BAG environment

        If the library do not exist, create it
        When created, check if this package has submodule OR class definition of \
        schematic

        If yes:

        1) Add: from import <design>.schematic import schematic to module definition \
            in BagModules/<design>/<templatename>.py
        2) Replace the parent class Module of the created Bag module with schematic \
           class of this package.
        3) In that module, replace the content of the class with "pass"

        If no:

        1) Copy generated module to <design>/schematic.py
        2) Change class name to schematic
        3) Relocate the Yaml file.

        The the steps above effectively this moves the schematic definition from \
        BagModules to <design>.schematic submodule. Making your module definition \
        independent of BAG installation location.

        """
        # Parameters

        # Import the templates
        self.print_log(msg='Importing netlist from virtuoso\n')

        # Path definitions
        thispath = os.path.dirname(os.path.realpath(self._classfile))
        new_lib_path = bag_project.bag_config['new_lib_path']
        schematic_generator = thispath + '/' + 'schematic.py'
        tempgenfile = thispath + '/' + 'schematic_temp.py'
        # Check if schematic generator already exists

        # packagename = bag_home + '/' + new_lib_path + '/' + template_library + '/' + cell + '.py'

        # NOTE: we now have an absolute new_lib_path set up in bag_config.yaml
        output_template_lib_path = os.path.abspath(os.path.join(new_lib_path, template_library))
        packagename = os.path.join(output_template_lib_path, cell + '.py')

        if os.path.isfile(packagename):
            newpackage = False
        else:
            newpackage = True
            os.makedirs(output_template_lib_path, exist_ok=True)
            self.print_log(msg="output template library path: %s" % output_template_lib_path)

        # Importing template library
        bag_project.import_design_library(template_library)

        # Schematic generator should be a submodule in THIS directory
        if not newpackage:
            # Schematic generator already existed
            self.print_log(msg='Schematic generator exists in %s' % packagename)

        elif os.path.isfile(schematic_generator):
            self.print_log(
                msg='Schematic generator exists at %s. Trying to map that to generated one.' % schematic_generator)
            # Test is schematic class exists in the schematic generator
            with open(schematic_generator, 'r') as generator_file:
                if 'class schematic(Module):' not in generator_file.read():
                    self.print_log(msg='Existing generator does not contain schematic class')
                    self.print_log(msg='Not compatible with this generator structure.\n Exiting')
                    quit()
                else:
                    self.print_log(msg='Mapping %s to generated class.' % schematic_generator)
                    # Here, figure out what to do with the generated module AND _new_ generator
                    ###inputfile = open(packagename, 'r').readlines()
                    inputfile = open(schematic_generator, 'r').readlines()

                    tempfile = open(tempgenfile, 'w')

                    for line in inputfile:
                        if re.match('from bag.design.module import Module', line) or re.match('from bag.design import Module', line):
                            tempfile.write('from %s.schematic import schematic as %s__%s\n' % (
                                self.package, template_library, cell))
                        else:
                            pass
                            # tempfile.write(line)
                    tempfile.close()
                    os.rename(tempgenfile, packagename)
                    self.print_log(msg='Created schematic generator at %s' % packagename)
                    self.print_log(msg='We need to re-run this to have mapped generators in effect')

                    warn("\nPlease refresh Virtuoso and rerun!\n")

                    quit()

        else:
            # Transfer schematic generator to thispath/schematic.py
            # One cell per directory. Import others from other generators
            os.path.dirname(os.path.realpath(self._classfile)) + "/" + __name__
            self.print_log(msg='Copying schematic generator to %s ' % (thispath + '/schematic.py'))
            shutil.copy2(packagename, schematic_generator)

            # First we generate a template to be transferred to
            # new_lib_path (BAGHOME/BagModules/template_library/cell.py
            inputfile = open(schematic_generator, 'r').readlines()
            tempfile = open(tempgenfile, 'w')

            for line in inputfile:
                if re.match('from bag.design.module import Module', line):
                    tempfile.write('from %s.schematic import schematic as %s__%s\n'
                                   % (cell, template_library, cell))
                else:
                    pass
            tempfile.close()
            # Move this to BagModules
            os.rename(tempgenfile, packagename)

            tempfile = open(tempgenfile, 'w')
            # Then rename the actual generator to class schematic
            for line in inputfile:
                if re.match('class ' + template_library + '__' + cell + '(Module):', line):
                    tempfile.write('class schematic(Module):\n')
                else:
                    tempfile.write(line)
            tempfile.close()
            os.rename(tempgenfile, schematic_generator)
            os.rename(tempgenfile, packagename)
            self.print_log(msg='You need to re-run this to have mapped generators in effect')

            log.warn("\nPlease refresh Virtuoso and rerun!\n")

            quit()
        #
        self.print_log(msg='Netlist import done')

