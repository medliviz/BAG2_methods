from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import *


class SimulationMode(str, Enum):
    __order__ = "SCHEMATIC POST_LAYOUT"

    SCHEMATIC = 'sch',
    POST_LAYOUT = 'rcx',

    @property
    def description(self):
        switcher = {
            SimulationMode.SCHEMATIC: 'schematic-level simulation',
            SimulationMode.POST_LAYOUT: 'post-layout simulation',
        }
        return switcher[self]
