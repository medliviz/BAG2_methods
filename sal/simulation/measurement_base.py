# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from typing import *

from sal.testbench_base import TestbenchBase
from sal.simulation.simulation_mode import SimulationMode


class MeasurementBase(ABC):
    #  post-process simulation results
    #     - post-processing (find or extract some specific/optimum stuff)
    #     - plotting

    # defaults could be:
    #   dc:       operating points of all transistors
    #   ac:       output VS freq
    #   ac_tran:  output VS time

    @abstractmethod
    def post_simulation_processing(self,
                                   testbench: TestbenchBase,
                                   mode: SimulationMode,
                                   hdf5_path: str):
        pass


def split_data_by_sweep(results: Dict,
                        var_list: List[str]) -> List[Tuple[str, Dict]]:
    sweep_names = results['sweep_params'][var_list[0]][:-1]
    combo_list = []
    for name in sweep_names:
        combo_list.append(range(results[name].size))

    if combo_list:
        idx_list_iter = product(*combo_list)
    else:
        idx_list_iter = [[]]

    ans_list = []
    for idx_list in idx_list_iter:
        cur_label_list = []
        for name, idx in zip(sweep_names, idx_list):
            swp_val = results[name][idx]
            if isinstance(swp_val, str):
                cur_label_list.append('%s=%s' % (name, swp_val))
            else:
                cur_label_list.append('%s=%.4g' % (name, swp_val))

        if cur_label_list:
            label = ', '.join(cur_label_list)
        else:
            label = ''

        cur_idx_list = list(idx_list)
        cur_idx_list.append(slice(None))

        cur_results = {var: results[var][tuple(cur_idx_list)] for var in var_list}
        ans_list.append((label, cur_results))

    return ans_list

