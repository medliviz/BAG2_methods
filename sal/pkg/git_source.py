from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
import git
import git.exc
from gitlab import Gitlab
import os
from typing import Dict, List, Optional
import urllib3

from sal.env import Env
from sal.log import *
from sal.pkg.package_base import PackageBase, PackageKind
from sal.pkg.package_factory import PackageFactory


class Protocol(Enum):
    HTTPS = "https",
    SSH = "ssh",


@dataclass
class GitSource:
    name: str
    protocol: Protocol
    git_server_url: str
    subgroup_path: str
    subgroup_id: int
    repo_blacklist: List[str] = ()
    git_access_token: Optional[str] = None  # personal token for GitLab

    def __post_init__(self):
        self._cached_packages = None

    @property
    def base_url(self) -> str:
        url = f"{self.git_server_url}/{self.subgroup_path}"
        return url

    def contains_url(self, url: str) -> bool:
        return url.startswith(self.base_url)

    def url_for_package(self, name: str) -> str:
        return f"{self.base_url}/{name}"

    @property
    def available_packages(self) -> [PackageBase]:
        if hasattr(self, '_cached_packages'):
            return self._cached_packages

        _cached_packages = []

        gl: Gitlab
        if self.git_access_token:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            gl = Gitlab(url=self.git_server_url, private_token=self.git_access_token, ssl_verify=False)
            gl.auth()
        else:
            gl = Gitlab(url=self.git_server_url)

        g = gl.groups.get(self.subgroup_id)
        projects = g.projects.list(as_list=False)
        for p in projects:
            name = p.name
            if name in self.repo_blacklist:
                continue  # skip incompatible packages
            url_key = {
                Protocol.HTTPS: 'http_url_to_repo',
                Protocol.SSH: 'ssh_url_to_repo',
            }
            _cached_packages.append(PackageFactory.package(
                name=name,
                url=getattr(p, url_key[self.protocol]),
                path=None,
                source=self
            ))

        return _cached_packages

    def init_repo(self, package: PackageBase):
        repo = git.Repo.init(package.path)
        origin = repo.create_remote('origin', self.url_for_package(package.name))

    @staticmethod
    def group_packages_by_kind(packages: [PackageBase]) -> Dict[PackageKind: [PackageBase]]:
        grouped = {}
        for package in packages:
            grouped.setdefault(package.kind(), [])
            pkg_list = grouped[package.kind()]
            pkg_list.append(package)
        return grouped


@dataclass
class GitSourceList:
    sources: List[GitSource]

    def source_by_name(self, name: str) -> GitSource:
        for s in self.sources:
            if s.name == name:
                return s
        return None

    def __iter__(self):
        return self.sources.__iter__()

    def __next__(self):
        return self.sources.__next__()

    def package_from_path(self, path: str) -> PackageBase:
        repo = git.Repo(path)
        origin_url = repo.remotes.origin.url
        for source in self.sources:
            if source.contains_url(origin_url):
                return PackageFactory.package(
                    name=os.path.basename(path),
                    url=origin_url,
                    path=path,
                    source=source
                )
        raise Exception(f"No git source found for package at path: {path}")

    def package_by_name(self,
                        name: str,
                        default_git_source: str) -> PackageBase:
        """
        Parameters
        ----------
        name:
            Full qualified name (i.e. mosaic/tgate_gen).
            The source name may be omitted

        default_git_source:
            If the source name is omitted in name (i.e. tgate_gen),
            the default git source will be used
        """

        source_name: str
        package_name: str

        parts = name.split('/')
        if len(parts) == 1:  # i.e. tgate_gen
            source_name = default_git_source
            package_name = name
        elif len(parts) == 2:  # i.e. mosaic/tgate_gen
            source_name = parts[0]
            package_name = parts[1]
        else:
            raise Exception(f"Invalid package name: {name}")
        source = self.source_by_name(source_name)
        if not source:
            raise Exception(f"Invalid git source: {source_name}")
        return PackageFactory.package(
            name=package_name,
            url=source.url_for_package(name=package_name),
            path=os.path.join(Env.generator_root, package_name),
            source=source
        )

    def installed_packages(self) -> [PackageBase]:
        files = os.listdir(Env.generator_root)
        pkg_dirs = list(filter(lambda name: PackageFactory.is_valid_package_filename(name), files))
        pkg_paths = list(map(lambda name: os.path.join(Env.generator_root, name), pkg_dirs))
        pkgs = []
        for path in pkg_paths:
            try:
                pkg = self.package_from_path(path)
                pkgs.append(pkg)
            except Exception as e:
                warn(f"Ignoring package at {path}, no source can be found\n")
        return pkgs

    def installed_packages_by_names(self,
                                    names: [str],
                                    quit_on_failure: bool = True) -> [PackageBase]:
        pkgs = []
        for name in names:
            path = os.path.join(Env.generator_root, name)
            try:
                pkg = self.package_from_path(path)
                pkg.validate_path()
                pkgs.append(pkg)
            except git.exc.NoSuchPathError as e:
                if quit_on_failure:
                    error(f"Package '{name}' was not added to file system")
                    info(f"\tNo directory exists at {e}")
                    sys.exit(1)
                else:
                    error(f"Skipping package '{name}' (not added to the file system): {e}")
            except Exception as e:
                if quit_on_failure:
                    error(f"Invalid package name '{name}': ", end='')
                    info(e)
                    sys.exit(1)
                else:
                    error(f"Skipping invalid package name '{name}': {e}")
        return pkgs

    def installed_package_by_name(self,
                                  name: str) -> PackageBase:
        found_pkgs = self.installed_packages_by_names(names=[name])
        return found_pkgs[0]
