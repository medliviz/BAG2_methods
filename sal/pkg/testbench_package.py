#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
import argparse
from dataclasses import dataclass
import sys
from typing import Dict, List, Optional

from sal.log import *
from sal.env import Env
from sal.pkg.package_base import PackageBase, PackageKind


class TestbenchPackage(PackageBase):
    @classmethod
    def kind(cls) -> PackageKind:
        return PackageKind(
            name="Testbench",
            suffix="_tb",
            package_class=cls
        )

    def __import_testbench_class(self):
        self.validate_path()

        # prepare python path
        from sal.bag_startup import BagStartup
        from sal.testbench_base import TestbenchBase
        BagStartup.setup_sys_path()

        import importlib.util
        python_module_name = f"{self.name}.testbench"
        if python_module_name in sys.modules:
            info(f"Module {python_module_name!r} was already imported!")
            module = sys.modules[python_module_name]
        else:
            spec = importlib.util.find_spec(python_module_name)
            if spec is None:
                raise Exception(f"Module {python_module_name!r} can't be located")
            module = importlib.util.module_from_spec(spec)
            sys.modules[python_module_name] = module
            spec.loader.exec_module(module)
        if not hasattr(module, 'testbench'):
            raise Exception(f"Module {python_module_name!r} must contain a class called 'testbench'")
        klass = getattr(module, 'testbench')
        if not issubclass(klass, TestbenchBase):
            raise Exception(f"Module {python_module_name!r} must be a subclass of {TestbenchBase}")

        info(f"Testbench class is {klass}\n")
        return klass

    @property
    def testbench_class(self) -> type[sal.design_base.TestbenchBase]:
        if not hasattr(self, '_cached_testbench_class'):
            self._cached_testbench_class = self.__import_testbench_class()
        return getattr(self, '_cached_testbench_class')

    def new_instance(self) -> sal.design_base.TestbenchBase:
        inst = self.testbench_class()
        inst.params = self.testbench_class.parameter_class().defaults()
        return inst



