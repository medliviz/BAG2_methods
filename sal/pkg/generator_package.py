#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
import argparse
from dataclasses import dataclass
import sys
from typing import Dict, List, Optional

from sal.log import *
from sal.env import Env
from sal.pkg.package_base import PackageBase, PackageKind


class GeneratorPackage(PackageBase):
    @classmethod
    def kind(cls) -> PackageKind:
        return PackageKind(
            name="Generator",
            suffix="_gen",
            package_class=cls
        )

    @property
    def generated_basename(self) -> str:
        return self.cell_name + "_generated" + "_" + Env.techlib

    def __import_design_class(self):
        self.validate_path()

        # prepare python path
        from sal.bag_startup import BagStartup
        from sal.design_base import DesignBase
        BagStartup.setup_sys_path()

        import importlib.util
        python_module_name = f"{self.name}.design"
        if python_module_name in sys.modules:
            info(f"Module {python_module_name!r} was already imported!")
            module = sys.modules[python_module_name]
        else:
            spec = importlib.util.find_spec(python_module_name)
            if spec is None:
                raise Exception(f"Module {python_module_name!r} can't be located")
            module = importlib.util.module_from_spec(spec)
            sys.modules[python_module_name] = module
            spec.loader.exec_module(module)
        if not hasattr(module, 'design'):
            raise Exception(f"Module {python_module_name!r} must contain a class called 'design'")
        gen_class = getattr(module, 'design')
        if not issubclass(gen_class, DesignBase):
            raise Exception(f"Module {python_module_name!r} must be a subclass of {DesignBase}")

        info(f"Generator class is {gen_class}\n")
        return gen_class

    @property
    def design_class(self) -> type[sal.design_base.DesignBase]:
        if not hasattr(self, '_cached_design_class'):
            self._cached_design_class = self.__import_design_class()
        return getattr(self, '_cached_design_class')

    def new_instance(self) -> sal.design_base.DesignBase:
        inst = self.design_class()
        inst.params = self.design_class.parameter_class().defaults(inst.min_lch)
        inst._implementation_library_name = self.generated_basename
        info(f"Generated (Output) library name: {inst.implementation_library_name}")
        return inst



