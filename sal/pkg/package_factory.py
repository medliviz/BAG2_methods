#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import Dict, List, Optional, Type

from sal.pkg.package_base import PackageBase, PackageKind
from sal.pkg.generator_package import GeneratorPackage
from sal.pkg.testbench_package import TestbenchPackage


class PackageFactory:
    @classmethod
    def all_package_kinds(cls) -> List[PackageKind]:
        return [
            PackageKind(
                name="Generator",
                suffix="_gen",
                package_class=GeneratorPackage
            ),

            PackageKind(
                name="Testbench",
                suffix="_tb",
                package_class=TestbenchPackage
            )
        ]

    @classmethod
    def package_kind_for_filename(cls, filename: str) -> Optional[PackageKind]:
        for kind in cls.all_package_kinds():
            if filename.endswith(kind.suffix):
                return kind
        return None

    @classmethod
    def is_valid_package_filename(cls, filename: str):
        return cls.package_kind_for_filename(filename) is not None

    @classmethod
    def package(cls,
                name: str,
                url: Optional[str],
                path: Optional[str],
                source: GitSource) -> PackageBase:
        kind = cls.package_kind_for_filename(name)
        # pkg: PackageBase = kind.package_class()
        # pkg.name = name
        # pkg.url = url
        # pkg.path = path
        # pkg.source = source
        pkg: PackageBase = kind.package_class(
            name=name,
            url=url,
            path=path,
            source=source
        )
        return pkg
