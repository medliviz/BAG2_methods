#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class

from abc import ABC, abstractmethod, abstractclassmethod
import sys
import os
from enum import Enum
from typing import *
import unittest
import yaml

from bag.util.cache import DesignMaster
from sal.env import Env


# -------------------------------------- Enums -------------------------------------

class DumpFormat(Enum):
    YAML = 'yaml'
    JSON = 'json'


# -------------------------------------- Classes -------------------------------------

class ParamsBase(ABC):
    @classmethod
    def parse_dump(cls,
                   path: str,
                   dump_format: Optional[DumpFormat]) -> type[GenericParamsBase]:
        if dump_format is None:
            _, suffix = os.path.splitext(path)
            dump_format = DumpFormat[suffix]
        if dump_format == DumpFormat.YAML:
            with open(path, 'r') as f:
                obj = yaml.load(f, Loader=yaml.Loader)
                return obj
        else:
            raise Exception(f"Dump format {dump_format} is not yet implemented")

    def dump(self,
             dump_format: DumpFormat,
             path: str = None) -> str:
        if dump_format == DumpFormat.YAML:
            if path is None:
                return yaml.dump(self, sys.stdout)
            else:
                with open(path, 'w') as f:
                    return yaml.dump(self, f)
        else:
            raise Exception(f"Dump format {dump_format} is not yet implemented")

    def get_immutable_key(self):
        """
        Required for bag.util.cache DesignMaster to_immutable_id()
        """
        return DesignMaster.to_immutable_id(self.__dict__)


class LayoutParamsBase(ParamsBase):
    @classmethod
    @abstractmethod
    def finfet_defaults(cls, min_lch):
        raise Exception("subclasses must implement this class method")

    @classmethod
    @abstractmethod
    def planar_defaults(cls, min_lch):
        raise Exception("subclasses must implement this class method")

    @classmethod
    def defaults(cls, min_lch):
        if Env.is_finfet():
            return cls.finfet_defaults(min_lch)
        else:
            return cls.planar_defaults(min_lch)


class GeneratorParamsBase(ParamsBase):
#    def layout_parameters(self) -> LayoutParamsBase:
#        raise Exception("subclasses must implement this method")

#    @property
#    def testbench_parameters(self) -> TestbenchParamsBase:
#        raise Exception("subclasses must implement this method")
    pass


# -------------------------------------- Unit Tests -------------------------------------

# TODO: no tests yet
class TestParamsBase(unittest.TestCase):
    def setUp(self):
        pass

    def test_dump(self):
        pass


if __name__ == '__main__':
    unittest.main()
