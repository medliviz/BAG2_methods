#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations  # allow class type hints within same class
import argparse
from dataclasses import dataclass
from enum import Enum
import os
import re
import shutil
import sys
import subprocess
from typing import Dict, List, Optional
from urllib.parse import urlparse

from sal.log import *
from sal.env import Env
from sal.gentool.config import Config
from sal.pkg.generator_package import GeneratorPackage
from sal.pkg.testbench_package import TestbenchPackage
from sal.simulation.simulation_mode import SimulationMode
from sal.checker.rcx import *


@dataclass
class SimulationSetup:
    generator_package: GeneratorPackage
    testbench_package: TestbenchPackage
    generator_instance: sal.design_base.DesignBase
    testbench_instance: sal.testbench_base.TestbenchBase
    simulation_mode: SimulationMode


class Args:
    def __init__(self, arg_list: List[str] = None):
        main_parser = argparse.ArgumentParser(description="Generator Tool")
        subparsers = main_parser.add_subparsers(dest="command", help="Sub-commands help")

        parser_config = subparsers.add_parser("config", help="Configuration")
        group = parser_config.add_mutually_exclusive_group()
        group.add_argument("--current", action="store_true", default=False, help="Dump current configuration")
        group.add_argument("--default", action="store_true", default=False, help="Dump default configuration")

        parser_avail = subparsers.add_parser("avail", help="List available packages (generators, testbenches)")
        parser_avail.add_argument("--verbose", "-v", dest="verbose", default=False, help="Verbose mode")

        parser_list = subparsers.add_parser("list", help="List installed packages (generators, testbenches)")
        parser_list.add_argument("--verbose", "-v", dest="verbose", default=False, help="Verbose mode")

        parser_add = subparsers.add_parser("add", help="Add package (generator, testbench)")
        parser_add.add_argument("--verbose", "-v", action="store_true", default=False, help="Verbose mode")

        group = parser_add.add_mutually_exclusive_group()
        group.add_argument("--all", "-a", dest="add_all", action='store_true', default=False, required=False,
                           help="Add all available generators")
        group.add_argument('packages', metavar='PACKAGE', type=str, nargs='*', default=[],
                           help='Package names (f.ex. mosaic/tgate_gen)')

        parser_new = subparsers.add_parser("new", help="Create a new generator directory")
        parser_new.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_new.add_argument("--ref", dest="reference_generator", default="mosaic/tgate_gen",
                                help="Reference generator to use as a starting point (default is mosaic/tgate_gen)")
        parser_new.add_argument("--overwrite", dest="overwrite", action="store_true", default=False,
                                help="Overwrite if already existent")
        parser_new.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                help='New generator name (f.ex. mosaic/my_fancy_gen)')
        #     parser_new.add_argument("--dry", "-d", dest="dry_run", action='store_true', default=False,
        #                             help="Dry run (no real changes to the file system)")

        parser_make = subparsers.add_parser("make", help="Build generator")
        parser_make.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                 help="Verbose mode")
        parser_make.add_argument("--path", "-p", dest="params_path", default=None, help="Input params file path")
        parser_make.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                 help='generator names (f.ex. tgate_gen)')

        parser_dump = subparsers.add_parser("dump", help="Dump default parameters as YAML")
        parser_dump.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                 help="Verbose mode")
        parser_dump.add_argument("--path", "-p", dest="params_path", default=None, help="Output params dump file path")
        parser_dump.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                 help='generator name (f.ex. tgate_gen)')

        parser_parse = subparsers.add_parser("parse", help="Parse parameters from YAML")
        parser_parse.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                  help="Verbose mode")
        parser_parse.add_argument("--path", "-p", dest="params_path", required=True, help="Input params file path")
        parser_parse.add_argument('generators', metavar='GENERATOR', type=str, nargs=1,
                                  help='generator name (f.ex. tgate_gen)')

        # parser_drc = subparsers.add_parser("drc", help="Run DRC on existing layout (Not yet implemented)")
        # parser_drc.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
        #                         help="Verbose mode")
        # parser_drc.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
        #                         help='generator names (f.ex. tgate_gen)')
        #
        parser_lvs = subparsers.add_parser("lvs", help="Run LVS on existing layout")
        parser_lvs.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_lvs.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                help='generator names (f.ex. tgate_gen)')

        parser_rcx = subparsers.add_parser("rcx", help="Run RCX on existing layout")
        # TODO: netlist formats
        #parser_rcx.add_argument("--format", "-f", dest="rcx_extraction_format", type=str,
        #                        help="Extraction netlist format")
        rcx_help = f"Extraction type ∈ {set([name for name, member in RCXExtractionType.__members__.items()])}."\
                   f"\nDefaults to {RCXExtractionType.DEFAULT.value}"
        parser_rcx.add_argument("--type", "-t", dest="rcx_extraction_type", type=str,
                                default=RCXExtractionType.DEFAULT.value,
                                help=rcx_help)
        parser_rcx.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_rcx.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
                                help='generator names (f.ex. tgate_gen)')

        parser_sim = subparsers.add_parser("sim", help="Run simulations")
        parser_sim.add_argument("--list", "-l", dest="list", action="store_true", default=False,
                                help="List available testbenches and simulation modes")
        parser_sim.add_argument("--mode", "-m", dest="sim_mode", type=str, default="sch",
                                help="Simulation Mode (sch/rcx), defaults to sch")
        parser_sim.add_argument("--testbench", "-t", dest="testbench", type=str, default="dc_tb",
                                help="Testbench (like dc_tb, ac_tran_tb, ac_tb, ...)")
        parser_sim.add_argument("--path", "-p", dest="params_path", default=None, help="Input params file path")
        parser_sim.add_argument("--only-postprocess", "-o", dest="only_postprocess", action="store_true", default=False,
                                help="Only postprocess an existing hdf5 result file")
        parser_sim.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
                                help="Verbose mode")
        parser_sim.add_argument('generators', metavar='GENERATOR', type=str, nargs='?',
                                help='generator name (f.ex. tgate_gen)')

        # parser_gds = subparsers.add_parser("gds", help="Export GDS for existing layout (Not yet implemented)")
        # parser_gds.add_argument("--verbose", "-v", dest="verbose", action="store_true", default=False,
        #                         help="Verbose mode")
        # parser_gds.add_argument("--path", "-p", dest="params_path", default=None, help="Output path")
        # parser_gds.add_argument('generators', metavar='GENERATOR', type=str, nargs='+',
        #                         help='generator names (f.ex. tgate_gen)')

    #     parser_xor = subparsers.add_parser("xor", help="Diff two GDS streams")
    #     parser_xor.add_argument("--generator", "-g", required=True, help="Path to the generator")
    #     parser_xor.add_argument('--old', dest="old_gds_path", required=True, help="Path to the old GDS")
    #     parser_xor.add_argument('--new', dest="new_gds_path", required=True, help="Path to the new GDS")

        if arg_list is None:
            arg_list = sys.argv[1:]

        args = main_parser.parse_args(arg_list)
        if args.command is None:
            main_parser.print_usage()
            sys.exit(1)
        self.command = args.command

        if self.command == 'add':
            self.add_all = args.add_all

        if self.command == 'new':
            self.overwrite = args.overwrite

        if Env.gen_tool_config_path:
            self.config = Config.load(Env.gen_tool_config_path)
        else:
            self.config = Config.default_config()

        self.verbose = getattr(args, "verbose", None)

        if self.command in ('make', 'dump', 'parse', 'new', 'lvs', 'drc', 'rcx'):
            # remove trailing slashes in generator names
            self.generators = list(map(lambda g: g[:-1] if g.endswith('/') else g,
                                       args.generators))

        if self.command in ('add',):
            # remove trailing slashes in generator names
            self.packages = list(map(lambda g: g[:-1] if g.endswith('/') else g,
                                       args.packages))

        if self.command == 'rcx':
            self.rcx_extraction_type = args.rcx_extraction_type

        if self.command in ('dump', 'parse', 'make', 'sim'):
            self.params_path = args.params_path

        if self.command in ('new',):
            self.reference_generator = args.reference_generator

        if self.command == 'config':
            self.dump_default_config = args.default
            self.dump_current_config = args.current
            if not self.dump_default_config and not self.dump_current_config:
                self.dump_current_config = True

        if self.command == 'sim':
            self.testbench = args.testbench
            self.sim_mode = SimulationMode(args.sim_mode)
            self.list = args.list
            if self.testbench is None:
                self.list = True
            if args.generators is None:
                self.generators = []
            elif isinstance(args.generators, str):
                self.generators = [args.generators]
            else:
                # remove trailing slashes in generator names
                self.generators = list(map(lambda g: g[:-1] if g.endswith('/') else g,
                                           args.generators))
            self.only_postprocess = args.only_postprocess
            if self.list:
                info("Available simulation modes:")
                for mode in SimulationMode:
                    info(f"\t- {mode.value} ... {mode.description}")
                info("\nAvailable testbenches:")
                info("\t<TODO> ... get this dynamically")
                info("\t- dc")
                info("\t...")
                sys.exit(0)


class Tool:
    def __init__(self,
                 args: Args):
        self.args = args

    def run(self):
        switcher = {
            'config': self.dump_configuration,
            'avail': self.list_available_packages,
            'list': self.list_installed_packages,
            'add': self.add_package,
            'new': self.new_generator,
            'make': self.make_generator,
            'lvs': self.run_lvs,
            'drc': self.run_drc,
            'rcx': self.run_rcx,
            'dump': self.dump_defaults,
            'parse': self.parse_parameters,
            'sim': self.run_sim,
        }
        switcher[self.args.command]()

    @property
    def git_sources(self) -> GitSourceList:
        return self.args.config.git_sources

    def dump_configuration(self):
        # Fallback to dumping default config if no custom config is available
        if self.args.dump_current_config:
            if Env.gen_tool_config_path is None or not os.path.exists(Env.gen_tool_config_path):
                self.args.dump_current_config = False
                self.args.dump_default_config = True

        config: Config

        if self.args.dump_current_config:
            TermColor.WHITE.print(f"Dumping config from {Env.gen_tool_config_path_variable}={Env.gen_tool_config_path}")
            TermColor.CLEAR.print(" ")
            config = self.args.config
        elif self.args.dump_default_config:
            with TermColor.WHITE:
                info(f"Dumping default config")
                info(f"\tNOTE: customize environmental variable {Env.gen_tool_config_path_variable} to a yaml file")
            TermColor.CLEAR.print(" ")
            config = Config.default_config()

        config.dump()

    def list_available_packages(self):
        from sal.pkg.git_source import GitSource
        installed_pkg_names = list(map(lambda g: g.name, self.git_sources.installed_packages()))

        info("Available packages to add:")
        for source in self.git_sources:
            pkgs_grouped_by_kind = GitSource.group_packages_by_kind(source.available_packages)
            for kind, packages in pkgs_grouped_by_kind.items():
                info(f"\n\t-------------------- {source.name} [{kind.name}] --------------------")
                for package in packages:
                    TermColor.WHITE.print(f"\tgen add {source.name}/{package.name}", end='')
                    if package.name in installed_pkg_names:
                        info("   (already installed)")
                    else:
                        info("")

        info("\nFor details please call:\n\tgen add --help")

    def list_installed_packages(self):
        with TermColor.WHITE:
            info("Installed packages:")
            names = list(map(lambda g: f"{g.source.name}/{g.name}", self.git_sources.installed_packages()))
            names.sort()
            for name in names:
                info(f"\t{name}")
            info("")

        info("To generate, please call:\n\tgen make <generator>")

    @staticmethod
    def cmd(args):
        info(f"Calling {' '.join(args)}")

        s = subprocess.run(args)
        if s.returncode != 0:
            raise Exception(f'Command failed with status code {s.returncode}')

    def add_package(self):
        if self.args.add_all:
            error("TODO: --all not yet implemented")
        else:
            for name in self.args.packages:
                pkg = self.git_sources.package_by_name(name=name,
                                                       default_git_source=self.args.config.default_git_source)
                self.add_single_package(pkg=pkg)

    @staticmethod
    def __patch(path: str,
                filter_pattern: str,
                prepend_lines: Optional[List[str]] = None,
                append_lines: Optional[List[str]] = None,
                use_set_semantics: bool = True,
                ):
        if prepend_lines is None:
            prepend_lines = []
        if append_lines is None:
            append_lines = []
        original_lines = []
        filtered_lines = []
        if os.path.exists(path):
            with open(path, 'r') as f:
                original_lines = f.readlines()
                # remove previous entries of this generator
                regex = re.compile(filter_pattern)
                filtered_lines = list(filter(lambda l: regex.search(l) is None, original_lines))
        new_lines = prepend_lines + filtered_lines + append_lines

        if use_set_semantics and set(original_lines) == set(new_lines):
            warn("\tNothing changed")
            return  # nothing to do

        tmp_path = path + ".new"
        with open(tmp_path, 'w') as f:
            f.writelines(new_lines)
        os.rename(tmp_path, path)

    def __patch_lib_files(self,
                          pkg: PackageBase,
                          step: int = 1):
        TermColor.WHITE.print(f"{step}) Patching cds.lib")
        step += 1

        info(f"\tat {Env.cds_lib_path}")

        pattern: str
        if isinstance(pkg, GeneratorPackage):
            pattern = r"^\s*DEFINE\s+(" + pkg.templates_basename + "|" + pkg.generated_basename + r")\s+.*$"
        elif isinstance(pkg, TestbenchPackage):
            pattern = r"^\s*DEFINE\s+(" + pkg.templates_basename + r")\s+.*$"
        else:
            pattern = ""

        lines = ["DEFINE %s $BAG_GENERATOR_ROOT/%s/%s\n" % (pkg.templates_basename, pkg.name, pkg.templates_basename)]
        self.__patch(path=Env.cds_lib_path,
                     filter_pattern=pattern,
                     append_lines=lines)

        TermColor.WHITE.print(f"{step}) Patching bags_lib.def")
        step += 1

        info(f"\tat {Env.bag_libs_def}")

        self.__patch(path=Env.bag_libs_def,
                     filter_pattern=r"^\s*(BAG_prim|" + pkg.templates_basename + r")\s+.*$",
                     prepend_lines=["BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives\n"],
                     append_lines=[f"{pkg.templates_basename} {Env.bag_modules_path_meta}\n"])

        TermColor.WHITE.print(f"{step}) Creating BagModules/{pkg.templates_basename}")
        step += 1

        python_templates = os.path.join(Env.generator_root, "BagModules", pkg.templates_basename)
        os.makedirs(python_templates, exist_ok=True)

        info("------------------------------------------------------")

    def add_single_package(self,
                           pkg: PackageBase):
        repo_path = os.path.join(Env.generator_root, pkg.name)

        info(f"Adding package {pkg.url} as {pkg.name} ...")

        step = 1
        TermColor.WHITE.print(f"{step}) Cloning GIT repo at {pkg.url}")
        step += 1

        if os.path.exists(repo_path):
            warn(f"\tAlready existent directory at {repo_path} is kept as-is")
        else:
            self.cmd(["git", "clone", pkg.url, repo_path])

        self.__patch_lib_files(pkg=pkg, step=step)

    # NOTE: previously, generators were launched using configure/Makefile
    #       which basically called the __init__.py script:
    #
    #           main_script = os.path.join(path, gen.name, "__init__.py")
    #           args = ["python3", main_script]
    #           if Env.is_finfet:  # TODO: make this tech agnostic!
    #               args.append("--finfet")
    #           self.cmd(args)
    #
    #       => in the SDF flow the generator module is expected to
    #           - have a 'design' module
    #           - the 'design' module contains a class called 'design'
    #           - this base class is derived from sal.design_base.DesignBase
    #

    def make_generator(self):
        self.validate_virtuoso_run_dir_exists()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = gen.new_instance()

            if self.args.params_path is not None:
                import sal.params_base
                inst.params = gen.design_class.parameter_class().parse_dump(path=self.args.params_path,
                                                                            dump_format=sal.params_base.DumpFormat.YAML)
                info(f"Using params from YAML file: {self.args.params_path}")

            inst.generate()

    def run_lvs(self):
        self.validate_virtuoso_run_dir_exists()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = gen.new_instance()
            inst.run_lvs()

    def run_drc(self):
        self.validate_virtuoso_run_dir_exists()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = gen.new_instance()
            inst.run_drc()

    def run_rcx(self):
        self.validate_virtuoso_run_dir_exists()

        for gen in self.git_sources.installed_packages_by_names(names=self.args.generators):
            inst = gen.new_instance()

            rcx_params = {}

            if self.args.rcx_extraction_type:
                rcx_params['netlist_type'] = self.args.rcx_extraction_type

            rcx_success, log_file = inst.bag_project.run_rcx(lib_name=inst.implementation_library_name,
                                                             cell_name=inst.name,
                                                             rcx_params=rcx_params)
            if rcx_success:
                info(f"RCX succeeded ({log_file})")
            else:
                error(f"Error: RCX failed, please check {log_file}")

    def dump_defaults(self):
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import sal.params_base

        gen = self.git_sources.installed_package_by_name(self.args.generators[0])
        inst = gen.new_instance()

        if self.args.params_path is None:  # print to stdout
            info(f"\nYAML dump of default parameters for {gen.name}:\n", flush=True)
            inst.params.dump(dump_format=sal.params_base.DumpFormat.YAML)
        else:
            inst.params.dump(dump_format=sal.params_base.DumpFormat.YAML, path=self.args.params_path)

    def parse_parameters(self):
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import sal.params_base

        gen = self.git_sources.installed_packages_by_name(self.args.generators[0])
        info(f"\nParse YAML dump for {gen.name}:\n", flush=True)
        obj = gen.design_class.parameter_class().parse_dump(path=self.args.params_path,
                                                            dump_format=sal.params_base.DumpFormat.YAML)
        print(obj)

    @staticmethod
    def validate_virtuoso_run_dir_exists():
        if not os.path.exists(Env.virtuoso_run_dir):
            error(f"Virtuoso run directory does not exist: {Env.virtuoso_run_dir_variable}={Env.virtuoso_run_dir}")
            info(f"(hint: perhaps the virtuoso tool-wrapper will set-up the directory)")
            sys.exit(1)

    def new_generator(self):
        self.validate_virtuoso_run_dir_exists()

        gen_name = self.args.generators[0]
        if not gen_name.endswith("_gen"):
            raise Exception(f"Generator name must end with '_gen'")
        gen = self.git_sources.package_by_name(name=gen_name,
                                               default_git_source=self.args.config.default_git_source)

        if os.path.exists(gen.path):
            if self.args.overwrite:
                shutil.rmtree(gen.path)
            else:
                error(f"Already existent directory at {gen.path}, please back up or remove it")
                sys.exit(1)

        step = 1

        TermColor.WHITE.print(f"{step}) Creating generator directory structure "
                              f"(based on {self.args.reference_generator})")
        step += 1
        info(f"\tat {gen.path}")

        os.makedirs(os.path.join(gen.path, gen.templates_basename))  # Virtuoso template

        prototype_gen: PackageBase
        prototype_gen = self.git_sources.package_by_name(name=self.args.reference_generator,
                                                         default_git_source="mosaic")

        # NOTE: generator name (i.e. tgate_gen) is the dir name for the Python Source
        tmp_dir = os.path.join(gen.path, 'tmp')
        tmp_git_clone = os.path.join(tmp_dir, prototype_gen.name)

        try:
            self.cmd(["git", "clone", prototype_gen.url, tmp_git_clone])

            # Copy Python scripts
            template_script_dir = os.path.join(tmp_git_clone, prototype_gen.name)
            new_script_dir = os.path.join(gen.path, gen.name)

            # Copy OA lib
            template_oa_library = os.path.join(tmp_git_clone, prototype_gen.templates_basename, prototype_gen.cell_name)
            new_oa_library = os.path.join(gen.path, gen.templates_basename, gen.cell_name)

            # copy files as a starting point for the new generator
            shutil.copytree(template_script_dir, new_script_dir)
            shutil.copytree(template_oa_library, new_oa_library)

            # patch python scripts to have the new name

            TermColor.WHITE.print(f"{step}) Patching Python files to use generator name {gen_name}")
            step += 1

            for root, dirs, files in os.walk(new_script_dir):
                for f in files:
                    if f.endswith('.py'):
                        script_path = os.path.join(root, f)
                        self.cmd(["sed", "-i", f's/{prototype_gen.cell_name}/{gen.cell_name}/g', script_path])

            TermColor.WHITE.print(f"{step}) Setting up GIT repo for {gen_name}")
            step += 1

            gen.source.init_repo(package=gen)

        finally:
            # sanity check, we don't want to remove something like /
            if len(tmp_git_clone) >= 2 and tmp_git_clone.startswith(gen.path):
                shutil.rmtree(tmp_dir, ignore_errors=True)

        self.__patch_lib_files(pkg=gen, step=step)

        warn(f"\nPlease update Virtuoso and add Symbol / Schematic to library {gen.templates_basename}!")

    def setup_simulation(self,
                         testbench: Testbench,
                         mode: SimulationMode,
                         generator: Generator) -> SimulationSetup:
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!

        import bag
        from sal.testbench_params import DUT
        from sal.params_base import DumpFormat

        gen = generator.new_instance()
        tb = testbench.new_instance()

        try:
            gen.validate_bag_server_availability()
        except Exception as e:
            gen.print_log(type="F", msg=f"{e}")

        supported_testbenches = gen.params.testbench_parameters.testbench_classes_by_name().keys()
        if testbench.name not in supported_testbenches:
            raise Exception(f"Generator {generator.name} does not support testbench {testbench.name}!")

        if self.args.params_path:
            gen_params: GeneratorParamsBase = gen.params.parse_dump(path=self.args.params_path,
                                                                    dump_format=DumpFormat.YAML)
            gen.params = gen_params

        return SimulationSetup(
            generator_package=generator,
            testbench_package=testbench,
            generator_instance=gen,
            testbench_instance=tb,
            simulation_mode=mode
        )

    def run_simulation(self,
                       testbench: Testbench,
                       mode: SimulationMode,
                       generator: Generator,
                       output_hdf5_path: str):
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import bag

        sim = self.setup_simulation(testbench=testbench,
                                    mode=mode,
                                    generator=generator)
        gen = sim.generator_instance
        tb = sim.testbench_instance

        gen_tb_def_params = gen.params.testbench_parameters.testbench_params_by_name
        tb_params = gen_tb_def_params[testbench.name]

        # Patch DUT info
        tb_params.dut.lib = generator.generated_basename
        tb_params.dut.cell = generator.cell_name

        if mode is SimulationMode.SCHEMATIC:
            view_name = 'schematic'
        elif mode is SimulationMode.POST_LAYOUT:
            view_name = "post_layout_TODO"  # TODO!!!
        elif mode is SimulationMode.RCX:
            view_name = "rcx_TODO"  # TODO!!!
        else:
            raise Exception(f"Unsupported simulation mode {mode}")

        sim_envs = ['TT']  # TODO!

        impl_lib = generator.generated_basename
        gen_cell = generator.cell_name

        tb_gen_cell = f"{generator.name}_{testbench.name}"  #"tgate_gen_dc_tb"

        step = 1
        ##__________________________________________________________________________________
        # import testbench templates (generate netlist if necessary)
        TermColor.WHITE.print(f"{step}) Importing testbench templates")
        step += 1

        tb.import_testbench_template(bag_project=gen.bag_project,
                                     template_library=testbench.templates_basename,
                                     cell=testbench.cell_name)

        ##__________________________________________________________________________________
        # create the testbench schematic
        TermColor.WHITE.print(f"\n{step}) Creating the testbench schematic")
        step += 1

        tb_dsn = gen.bag_project.create_design_module(lib_name=testbench.templates_basename,
                                                      cell_name=testbench.cell_name)
        tb_dsn.design(dut_lib=impl_lib,
                      dut_cell=gen_cell,
                      params=tb_params)
        tb_dsn.implement_design(lib_name=impl_lib,
                                top_cell_name=tb_gen_cell)
        ##__________________________________________________________________________________
        TermColor.WHITE.print(f"\n{step}) Setting up ADEXL state")
        step += 1

        # setup testbench ADEXL state
        bag_tb = gen.bag_project.configure_testbench(
            tb_lib=impl_lib,
            tb_cell=tb_gen_cell
        )
        info('tb object created!')

        sim_params = tb_params.simulator_params()

        # set testbench parameters values
        for key, val in sim_params.items():
            bag_tb.set_parameter(key, val)
        info('parameters are set!')

        # set config view, i.e. schematic vs extracted
        bag_tb.set_simulation_view(impl_lib, gen_cell, view_name)
        info(f'changed the config view to {view_name}!')

        # set process corners
        bag_tb.set_simulation_environments(sim_envs)
        info(f'set tb environment to {sim_envs}')

        # commit changes to ADEXL state back to database
        bag_tb.update_testbench()
        info('updated changes to ADEXL')

        info(f"Output directories:\n\tTestbench State: {bag_tb.save_dir}\n\t")

        # start simulation
        TermColor.WHITE.print(f"{step}) Running simulation")
        step += 1
        bag_tb.run_simulation()

        # import simulation results to Python
        info('simulation done, load results')
        results = bag.data.load_sim_results(bag_tb.save_dir)

        # save simulation data as HDF5 format
        TermColor.WHITE.print(f"{step}) Saving simulation result data")
        step += 1
        bag.data.save_sim_results(results, output_hdf5_path)
        info(f"Output file: {output_hdf5_path}")
        info("Finished simulations.")

    def post_simulation_processing(self,
                                   testbench: Testbench,
                                   mode: SimulationMode,
                                   generator: Generator,
                                   hdf5_path: str):
        from sal.bag_startup import BagStartup
        BagStartup.setup_sys_path()  # NOTE: add frameworks to sys.path, this influences the following imports!
        import bag
        from sal.simulation.measurement_base import MeasurementBase

        sim = self.setup_simulation(testbench=testbench,
                                    mode=mode,
                                    generator=generator)
        gen = sim.generator_instance
        tb = sim.testbench_instance

        if not hasattr(gen, 'measurement_class'):
            error(f"{generator.name} does not support measurements")
            sys.exit(1)

        kls = gen.measurement_class()
        measurement_instance: MeasurementBase = kls()
#        try:
        measurement_instance.post_simulation_processing(testbench=tb,
                                                        mode=mode,
                                                        hdf5_path=hdf5_path)
#        except Exception as e:
#            error(f"Post-simulation processing failed for {generator.name} due to exception: {e}")
        info("Finished post-simulation processing.")

    def run_sim(self):
        self.validate_virtuoso_run_dir_exists()

        generator = self.git_sources.installed_package_by_name(self.args.generators[0])

        #  ___________________________________________________________________________
        # TODO: dynamically discover available/installed testbenches!
        testbench: TestbenchPackage
        for candidate in ('dc_tb', 'ac_tran_tb', 'ac_tb'):
            if self.args.testbench == candidate or self.args.testbench is None:
                testbench = TestbenchPackage(
                    name=candidate,
                    url=None,
                    path=os.path.join(Env.generator_root, candidate),
                    source=None  # TODO
                )
        if testbench is None:
            error(f"Testbench {self.args.testbench} is not yet supported!")
            sys.exit(1)
        #  ___________________________________________________________________________

        tb_gen_cell = f"{generator.name}_{testbench.name}"  # i.e. "tgate_gen_dc_tb"

        data_dir = os.path.join(Env.virtuoso_run_dir,
                                tb_gen_cell,
                                "simulation_data")
        info(f"Simulation Result Data: {data_dir}")

        hdf5_path = os.path.join(data_dir, '%s.hdf5' % tb_gen_cell)

        if self.args.only_postprocess:
            if not os.path.exists(hdf5_path):
                error(f"Postprocessing is not possible because simulation results do not exist at {hdf5_path}")
        else:
            self.run_simulation(testbench=testbench,
                                mode=self.args.sim_mode,
                                generator=generator,
                                output_hdf5_path=hdf5_path)

        self.post_simulation_processing(testbench=testbench,
                                        mode=self.args.sim_mode,
                                        generator=generator,
                                        hdf5_path=hdf5_path)


def main():
    args = Args()

    if args.verbose:
        info("Called with environmental variables: %s" % Env.__dict__)
        info("------------------------------------------------------")
        info("Called with Arguments: %s" % args.__dict__)
        info("------------------------------------------------------")

    tool = Tool(args=args)
    tool.run()


if __name__ == '__main__':
    main()
