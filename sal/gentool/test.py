#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from .tool import Args, Tool


class GentoolTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_no_args(self):
        with self.assertRaises(SystemExit) as se:
            Args([])
        self.assertEqual(se.exception.code, 1)

    def test_help(self):
        with self.assertRaises(SystemExit) as se:
            Args(["--help"])
        self.assertEqual(se.exception.code, 0)

    def test_avail(self):
        args = Args(['avail'])
        t = Tool(args=args)
        t.run()

    def test_list(self):
        args = Args(['list'])
        t = Tool(args=args)
        t.run()

    def test_config(self):
        args = Args(['config'])
        t = Tool(args=args)
        t.run()

    def test_add(self):
        args = Args(['add', 'mosaic/amp_cs_gen'])
        t = Tool(args=args)
        t.run()

    def test_dump(self):
        args = Args(['dump', 'amp_cs_gen'])
        t = Tool(args=args)
        t.run()

    def test_amp_cs_gen(self):
        args = Args(['make', 'amp_cs_gen'])
        t = Tool(args=args)
        t.run()

        args = Args(['lvs', 'amp_cs_gen'])
        t = Tool(args=args)
        t.run()

    def test_sim_list(self):
        with self.assertRaises(SystemExit) as se:
            Args(['sim', '--list'])
        self.assertEqual(se.exception.code, 0)

    def test_sim_dc_tb(self):
        args = Args(['sim', '-t', 'dc_tb', 'amp_cs_gen'])
        t = Tool(args=args)
        t.run()

    def test_new(self):
        args = Args(['new', '--overwrite', 'sal/test3_gen'])
        t = Tool(args=args)
        t.run()


if __name__ == '__main__':
    unittest.main()
