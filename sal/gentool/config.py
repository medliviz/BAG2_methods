from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Optional
import yaml

from sal.env import Env
from sal.log import *
from sal.pkg.git_source import *


@dataclass
class Config:
    git_sources: GitSourceList
    default_git_source: str

    @classmethod
    def default_config(cls) -> Config:
        return Config(
            git_sources=GitSourceList([
                GitSource(name="mosaic",
                          protocol=Protocol.HTTPS,
                          git_server_url="https://gitlab.com",
                          subgroup_path="mosaic_group/mosaic_BAG/generators",
                          subgroup_id=52541524,
                          repo_blacklist=['inverter_gen']),
            ]),
            default_git_source="mosaic"
        )

    @classmethod
    def load(cls, path: str) -> Config:
        with open(path, 'r') as f:
            obj = yaml.load(f, Loader=yaml.Loader)
            return obj

    def dump(self,
             path: str = None) -> str:
        if path is None:
            return yaml.dump(self, sys.stdout)
        else:
            with open(path, 'w') as f:
                return yaml.dump(self, f)
