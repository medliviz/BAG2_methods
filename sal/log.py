#! /usr/bin/env python3

import datetime
from enum import Enum
import logging
import pprint
import sys
import unittest


# --------------------------------------- terminal colors ----------------------------------------
class TermColor(Enum):
    CLEAR = 1  # used for reset
    RED = 2
    GREEN = 3
    YELLOW = 4
    BLUE = 5,
    WHITE = 6,

    @property
    def termcode(self) -> str:
        # see https://misc.flogisoft.com/bash/tip_colors_and_formatting
        termcodes = {
            TermColor.RED: '\033[1;31m',
            TermColor.GREEN: '\033[32m',
            TermColor.YELLOW: '\033[93m',
            TermColor.BLUE: '\033[94m',
            TermColor.WHITE: '\033[97m',
            TermColor.CLEAR: '\033[37m',  # '\033[0m',
        }
        return termcodes.get(self, None)

    @staticmethod
    def clear():
        TermColor.CLEAR.set()

    def set(self):
        if self.termcode is None:
            raise Exception("Color is not supported:", self)
        print(f'{self.termcode}', file=sys.stderr, end='')

    def print(self, *args, **kwargs):
        self.set()
        if 'file' in kwargs:
            print(*args, **kwargs)
        else:
            print(*args, file=sys.stderr, **kwargs)
        self.clear()

    # NOTE: to be able to use the with-syntax:
    #       with TermColor.BLUE as t:
    #           print("msg1")
    #           print("msg2")
    #       2 methods are required: __enter__ and __exit__
    def __enter__(self):
        self.set()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.clear()


# --------------------------------------- logging ----------------------------------------

class ColorFormatter(logging.Formatter):
    """
    Logging formatter with colored output
    """
    def __init__(self, fmt):
        super().__init__()
        self.fmt = fmt
        self.color_fmt_by_level = {
            logging.DEBUG: TermColor.GREY + self.fmt + TermColor.CLEAR,
            logging.INFO:  TermColor.WHITE + self.fmt + TermColor.CLEAR,
            logging.WARN:  TermColor.YELLOW + self.fmt + TermColor.CLEAR,
            logging.ERROR: TermColor.RED + self.fmt + TermColor.CLEAR,
            logging.CRITICAL: TermColor.BOLD_RED + self.fmt + TermColor.CLEAR,
        }

    def format(self, record: logging.LogRecord):
        fmt = self.color_fmt_by_level[record.levelno]
        formatter = logging.Formatter(fmt)
        return formatter.format(record)


def logger(name: str, file: str) -> logging.Logger:
    lgr = logging.getLogger(name)
    lgr.setLevel(logging.DEBUG)

    stderr_handler = logging.StreamHandler()
    stderr_handler.setFormatter(ColorFormatter(fmt='%(asctime)s | %(levelname)8s | %(message)s'))
    stderr_handler.setLevel(logging.DEBUG)

    file_handler = logging.FileHandler(file)
    file_handler.setLevel(logging.DEBUG)

    lgr.addHandler(stderr_handler)
    lgr.addHandler(file_handler)

    return lgr


# --------------------------------------- logging ----------------------------------------
def info(*args, **kwargs):
    if 'file' in kwargs:
        print(*args, **kwargs)
    else:
        print(*args, file=sys.stderr, **kwargs)


def error(*args, **kwargs):
    TermColor.RED.print(*args, **kwargs)


def success(*args, **kwargs):
    TermColor.GREEN.print(*args, **kwargs)


def warn(*args, **kwargs):
    TermColor.YELLOW.print(*args, **kwargs)


def dump(msg: str, o: object):
    print("__________________________________________________")
    warn(msg)
    pprint.pprint(o)
    print(".")


# --------------------------------------- main ----------------------------------------
class TestLogging(unittest.TestCase):
    def test_success(self):
        success("test success")

    def test_error(self):
        error("test error")

    def test_warn(self):
        warn("test warning")

    def test_dump(self):
        dump("some dict", {'k1': 'v1', 'k2': 'v2'})

    def test_blue(self):
        TermColor.BLUE.print("Blue text", "(additional parameter)")

    def test_white(self):
        TermColor.WHITE.print("White text", "(additional parameter)")


if __name__ == '__main__':
    unittest.main()
