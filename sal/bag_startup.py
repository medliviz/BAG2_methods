"""
Cleaned-Up BAG startup,
somewhat based on bag_ecd/bag_startup.py
"""

import sys
import os
import time
import tempfile
import getpass
import abc
import yaml
from typing import Dict

from sal.env import Env


def setup_default_logfile() -> str:
    logfile = "/tmp/BAG_" + os.path.basename(tempfile.mkstemp()[1]) + "_" + \
                   getpass.getuser() + "_" + time.strftime("%Y%m%d%H%M") + ".log"
    if os.path.isfile(logfile):
        os.remove(logfile)
    # print("Setting default logfile %s" % logfile, file=sys.stderr)
    return logfile

def parse_bag_config() -> Dict:
    config = os.path.join(Env.bag_src_root, "bag_config.yaml")
    with open(config, 'r') as f:
        config_dict = yaml.load(f, Loader=yaml.FullLoader)
        return config_dict

class BagStartup(metaclass=abc.ABCMeta):
    #
    # class fields
    #
    BAGHOME = Env.bag_src_root
    BAG_GENERATOR_ROOT = Env.generator_root
    logfile = setup_default_logfile()
    bag_config = parse_bag_config()

    @staticmethod
    def __find_generators(roots: [str]) -> [str]:
        generators = []
        for root in roots:
            for item in os.listdir(root):
                if not os.path.isfile(os.path.join(root, item)):
                    if os.path.isfile(os.path.join(root, item, item, '__init__.py')):
                        generators.append(os.path.join(root, item))
        return generators

    @classmethod
    def setup_sys_path(cls,
                       verbose: bool = False):
        if verbose:
            print("--------------[Setting up BAG environmental variables]---------------")
        sys.path.append(Env.bag_framework)
        sys.path.append(Env.bag_tech_config_dir)
        sys.path.append(os.path.join(Env.bag_tech_config_dir, 'BAG_prim/layouts'))
        sys.path.append(os.path.join(cls.BAGHOME, 'BAG2_templates'))

        generators = cls.__find_generators(roots=[
            cls.BAGHOME,
            cls.BAG_GENERATOR_ROOT,  # additional generators from outside the repo
        ])
        generators_not_yet_in_PATH = list(set(generators) - set(sys.path))
        for path in generators_not_yet_in_PATH:
            if verbose:
                print("Adding %s to system path" % path)
            sys.path.append(path)
        if verbose:
            print("---------------------------------------------------------------------------\n")

