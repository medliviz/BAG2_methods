#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import *
import unittest
from bag.layout.routing import TrackID
from abs_templates_ec.analog_core import AnalogBase

#-------------------------------------- Enums -------------------------------------

#... (no enums yet)

#-------------------------------------- Classes -------------------------------------

class RoutingGridHelper:
    """
    Initialize the routing grid helper.

    Parameters
    ----------
    analog_base: AnalogBase
        analog base instance
    unit_mode : bool
        True if the given coordinate is in resolution units.
    layer_id : int
        the layer number.
    track_width : int
        width of one track in number of tracks.
    """
    def __init__(self,
                 analog_base: AnalogBase,
                 unit_mode: bool,
                 layer_id: int,
                 track_width: int,
                 ):
        self.analog_base = analog_base
        self.unit_mode = unit_mode
        self.layer_id = layer_id
        self.track_width = track_width

    def track_by_col(self,
                     col_idx: int,
                     offset: int = 0,
                     half_track: bool = False,
                     ) -> TrackID:
        """
        Returns the track for a given column index

        Parameters
        ----------
        col_idx : int
            the transistor index.  0 is left-most transistor.
        offset: int
            offset to the track index
        half_track : bool
            if True, allow half integer track numbers.

        Returns
        -------
        track : TrackID
            the track
        """
        return TrackID(
            layer_id=self.layer_id,
            track_idx=self.analog_base.grid.coord_to_nearest_track(
                layer_id=self.layer_id,
                coord=self.analog_base.layout_info.col_to_coord(
                    col_idx=col_idx,
                    unit_mode=self.unit_mode
                ),
                half_track=half_track,
                mode=1,
                unit_mode=self.unit_mode
            ) + offset,
            width=self.track_width
        )

#-------------------------------------- Unit Tests -------------------------------------

class TestRoutingGridHelper(unittest.TestCase):
    def setUp(self):
        pass

    # NOTE: no tests yet
    # def test_foo(self):
    #     self.assertEqual(....)

if __name__ == '__main__':
    unittest.main()
