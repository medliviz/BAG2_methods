from __future__ import annotations  # allow class type hints within same class
import abc
from dataclasses import dataclass
from typing import *

from sal.env import Env
from sal.params_base import ParamsBase


@dataclass
class SignalSource(ParamsBase):
    source_name: str
    plus_net_name: str
    minus_net_name: str
    bias_value: Union[str, float]

    def to_bag_dict(self) -> Dict[str, List[str]]:
        """
        Returns
        -------
        a dictionary prepared for calling bag.design.Module.design_dc_bias_sources()
        """
        return {
            self.source_name: [self.plus_net_name, self.minus_net_name, self.bias_value]
        }


@dataclass
class DUTTerminal(ParamsBase):
    term_name: str
    net_name: Union[str, List[str]]


@dataclass
class TestbenchSchematicParams(ParamsBase):
    dut_conns: List[DUTTerminal]
    v_sources: List[SignalSource]
    i_sources: List[SignalSource]
    instance_cdf_parameters: Dict[str, Dict]

    def to_bag_vbias_dict(self) -> Dict[str, List[str]]:
        vbias_dict = {}
        for v_source in self.v_sources:
            vbias_dict.update(v_source.to_bag_dict())
        return vbias_dict

    def to_bag_ibias_dict(self) -> Dict[str, List[str]]:
        ibias_dict = {}
        for i_source in self.i_sources:
            ibias_dict.update(i_source.to_bag_dict())
        return ibias_dict


@dataclass
class DUT(ParamsBase):
    lib: str
    cell: str

    @classmethod
    def placeholder(cls) -> DUT:
        """
        The DUT is part of the testbench parameters,
        but set up by the framework depending on the generated library.
        Therefor a placeholder is used for the default parameters.
        """
        return DUT(lib="lib", cell="cell")


@dataclass
class TestbenchParamsBase(ParamsBase):
    dut: DUT
    sch_params: TestbenchSchematicParams

    def simulator_params(self) -> Dict:
        blacklist = {'dut', 'sch_params'}
        remainder = dict(filter(lambda e: e[0] not in blacklist, self.__dict__.items()))
        return remainder
