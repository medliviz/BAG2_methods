#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from enum import Enum
from typing import *
from operator import attrgetter
import unittest

#-------------------------------------- Enums -------------------------------------

class ChannelType(Enum):
    N = 'nch'
    P = 'pch'

class RowOrientation(Enum):
    R0 = 'R0'
    MX = 'MX'

#-------------------------------------- Classes -------------------------------------

class Row:
    '''
    Parameters
    ----------
    name : str
        name of the row
    orientation : RowOrientation
        either 'R0' or 'MX' to specify whether the row is oriented gate down or gate up
    channel_type : ChannelType
        either 'nch' to specify an nch row or 'pch' to specify a pch row
    width : float
        width
    threshold: str
        threshold flavor
    wire_names_g: [str]
        wire names for g
    wire_names_ds: [str]
        wire names for ds
    '''

    def __init__(self,
                 name: str,
                 orientation: RowOrientation,
                 channel_type: ChannelType,
                 width: float,
                 threshold: str,
                 wire_names_g: [str],
                 wire_names_ds: [str],
                 ):
        self.name = name
        self.orientation = orientation
        self.channel_type = channel_type
        self.width = width
        self.threshold = threshold
        self.wire_names_g = wire_names_g
        self.wire_names_ds = wire_names_ds

class RowList:
    '''
    Parameters
    rows : List[Row]
        Given an ordered list of rows (from bottom of design to top)
        To abide by analogBase restrictions, all nch rows must come before pch rows
    ----------
    '''
    def __init__(self,
                 rows: List[Row],
                 validate_order=True):
        self.rows = rows
        if validate_order:
            self.__validate_order()

    def __len__(self):
        return len(self.rows)

    def rows_of_channel_type(self, channel_type: ChannelType) -> RowList:
        ch_rows = list(filter(lambda r: r.channel_type is channel_type, self.rows))
        return RowList(rows=ch_rows, validate_order=False)

    @property
    def n_rows(self) -> RowList:
        return self.rows_of_channel_type(ChannelType.N)

    @property
    def p_rows(self) -> RowList:
        return self.rows_of_channel_type(ChannelType.P)

    def index_of_same_channel_type(self, row: Row) -> int:
        same_channel_list = self.rows_of_channel_type(channel_type=row.channel_type)
        return same_channel_list.rows.index(row)

    def attribute_values(self, name: str) -> List:
        def __get_value(row):
            v = attrgetter(name)(row)
            if isinstance(v, Enum):
                return str(v.value)
            return v
        return list(map(__get_value, self.rows))

    def __wire_names(self):
        return list(map(lambda r: dict(
                            g=r.wire_names_g,
                            ds=r.wire_names_ds
                        ),
                        self.rows))

    def wire_names_dict(self) -> Dict[str, [Dict[str, [str]]]]:
        return dict(
            nch=self.n_rows.__wire_names(),
            pch=self.p_rows.__wire_names()
        )

    def __validate_order(self) -> None:
        # Validate parameters:
        #   To abide by analogBase restrictions, all nch rows must come before pch rows
        n_idxs = list(map(lambda r: self.rows.index(r), self.n_rows.rows))
        p_idxs = list(map(lambda r: self.rows.index(r), self.p_rows.rows))
        if len(n_idxs) < 1: return
        if len(p_idxs) < 1: return
        if max(n_idxs) > min(p_idxs):
            raise ValueError("all nch rows must come before pch rows")

#-------------------------------------- Unit Tests -------------------------------------

class TestRowList(unittest.TestCase):
    def setUp(self):
        self.n1 = Row(name="n", orientation=RowOrientation.R0,
                      channel_type=ChannelType.N, width=1, threshold='lvt',
                      wire_names_g=['sig1', 'EN'],
                      wire_names_ds=['I'])
        self.n2 = Row(name="n", orientation=RowOrientation.R0,
                      channel_type=ChannelType.N, width=2, threshold='lvt',
                      wire_names_g=['sig1', 'EN'],
                      wire_names_ds=['I', 'O'])
        self.p1 = Row(name="p", orientation=RowOrientation.R0,
                      channel_type=ChannelType.P, width=3, threshold='lvt',
                      wire_names_g=['ENB'],
                      wire_names_ds=['I', 'O'])

    def test_n_rows(self):
        rows = RowList([self.n1, self.n2, self.p1])
        self.assertEqual(len(rows.n_rows), 2)

    def test_p_rows(self):
        rows = RowList([self.n1, self.n2, self.p1])
        self.assertEqual(len(rows.p_rows), 1)

    def test_attribute_values(self):
        rows = RowList([self.n1, self.n2, self.p1])
        self.assertListEqual(rows.attribute_values(name='width'), [1, 2, 3])

    def test_attribute_values_enums(self):
        rows = RowList([self.n1, self.n2, self.p1])
        self.assertListEqual(rows.attribute_values(name='orientation'), ['R0', 'R0', 'R0'])

    def test_validate_order_valid(self):
        rows = RowList(rows=[self.n1, self.n2, self.p1],
                       validate_order=True)

    def test_validate_order_invalid(self):
        with self.assertRaises(ValueError):
            rows = RowList(rows=[self.n1, self.p1, self.n2],
                           validate_order=True)

    def test_wire_names_dict(self):
        rows = RowList(rows=[self.n1, self.n2, self.p1])
        obtained = rows.wire_names_dict()
        expected = dict(
            nch=[
                dict(
                    g=['sig1', 'EN'],
                    ds=['I']
                ),
                # nmos row
                dict(
                    g=['sig1', 'EN'],
                    ds=['I', 'O']
                ),
            ],
            pch=[
                dict(
                    g=['ENB'],
                    ds=['I', 'O']
                ),
            ]
        )
        self.assertEqual(obtained, expected)

if __name__ == '__main__':
    unittest.main()
