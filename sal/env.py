#! /usr/bin/env python3

import os
import unittest

# --------------------------------------- classes ----------------------------------------


class Env:
    bag_src_root = os.environ['BAG_WORK_DIR']

    user_home = os.environ['USER_HOME_DIR']
    virtuoso_run_dir_variable = 'VIRTUOSO_DIR'
    virtuoso_run_dir = os.environ[virtuoso_run_dir_variable]
    generator_root = os.environ['BAG_GENERATOR_ROOT']

    techlib = os.environ['TECHLIB']

    bag_framework = os.environ['BAG_FRAMEWORK']
    bag_tech_config_dir = os.environ['BAG_TECH_CONFIG_DIR']

    gen_tool_config_path_variable = "GEN_TOOL_CONFIG_PATH"

    # derived fields
    cds_lib_path = os.path.abspath(os.path.join(virtuoso_run_dir, "cds.lib"))
    bag_modules_path = os.path.join(generator_root, "BagModules")
    bag_modules_path_meta = "${BAG_GENERATOR_ROOT}/BagModules"
    bag_libs_def = os.path.join(generator_root, "bag_libs.def")
    gen_tool_config_path = os.environ.get(gen_tool_config_path_variable, None)

    @staticmethod
    def is_finfet() -> bool:  # TODO: make this tech agnostic!
        return Env.techlib == "cds_ff_mpt"  # TODO: make this tech agnostic!

# --------------------------------------- main ----------------------------------------


class TestEnv(unittest.TestCase):
    def test_instantiate(self):
        print(Env.__dict__)


if __name__ == '__main__':
    unittest.main()
